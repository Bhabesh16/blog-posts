---
title: Truthy and Falsey values in JavaScript
date: 2023-02-22 14:11:09
tags:
---


## **Introduction**

In this article, we’ll explore the concept of truthy and falsey values in JavaScript with code examples. we’ll also understand what makes a value truthy or falsey, how JavaScript makes its comparisons, as well as how truthy and falsey values can help you write cleaner code.

<img src="https://i.ytimg.com/an/HiwZV3vJdhk/18273570947243289559_mq.jpg?v=60522fd8" width=25% >

## **Truthy and falsey values**

Truthy values are expressions that evaluate the boolean value of true, and falsey values are expressions that evaluate to the boolean value of false. Some of the rules determine how non-Boolean values are translated into true or false values. Understanding the concepts and their effect on comparison helps when debugging JavaScript applications.

The following values are always falsey:

- `false`
- `0`
- `-0 `
- `0n `
- `'', "", `` (empty string)`
- `null`
- `undefined`
- `NaN`

Everything else is truthy. That includes:

- `'0'` string with text "zero"
- `'false' ` string with text “false”
- `[]` an empty array
- `{}` an empty object
- `function(){...}` an “empty” function

- ### **Falsey Values are as follows** :-

  - ### **false**

    ``` Js
    if(false) {
    console.log("Yes!!");
    }
    ```
    In the example above, Our code editor will not show any code. This is because the if statement evaluates the condition to `true`, and because the if has `false`, the log statement would never be displayed.

  - ### **0 (zero)**

    ``` Js
    if(0) {
        console.log("Yes!!");
    }
    else {
        console.log("No!!");
    }
    ```

    In the example above, The number `0` is also evaluated to a `false`. `no` will be desplayed because `0` is `false`, so the else part will be run.

  - ### **‘ ’ or “ ” (empty string)**

    ``` Js
    if("") {
        console.log("Yes!!");
    }
    else {
        console.log("No!!");
    }
    ```

    In the above example, An empty string is evaluated to be `false`. and again, `no` will be shown as a display.

  - ### **null**

    `null` is evaluated to be `false`. It is used when we don’t want to assign a value yet to a variable. The type of on `null` returns an object. So, if you want to check whether a variable initialized with `null` has been initialized with a value, `null` will return `false`.

    ``` Js
    let test = null;

    if(test) {
        console.log("Yes, testing done");
    }
    else {
        console.log("No, not done yet");
    }

    test = 56;

    if(test) {
        console.log("Yes, test contains a value");
    }
    else {
        console.log("No, not done yet");
    }
    ```
    In the above example, At first, `No, not done yet` will be displayed, and second, when the value of the test is being updated to a number then `Yes, testing is done` will be displayed

  - ### **undefined**

    `undefined` is assigned to a variable whose value has not been initialized. It resolves to false.

    ``` Js
    let test;

    if(test) {
     console.log("yes, testing variable is done");
    }
    else {
     console.log("no, not done yet");
    }

    test = 105;

    if(test) {
     console.log("yes, initialized to a number");
    }
    else {
     console.log("no, not done yet");
    }
    ```
    In the above example, At first, `No, not done yet` will be displayed, and second, when the value of the test is being updated to a number then `Yes, initialized to a number` will be displayed

  - ### **NaN**
  This is a number just like `0`, and it resolves to `false`. `NaN` occurs when a number is divided by another data type.

- ### **Truthy Values are as follows** :-

  - ### **'0' and 'false'(string contains text)**
    ``` Js
    let test = '0';

    if(test) {
        console.log("Yes!!");
    }
    else {
        console.log("No!!");
    }
    
    test = 'false';
    
    if(test) {
        console.log("Yes!!");
    }
    else {
        console.log("No!!");
    }
    ```

    In both the if-else statement, Our code editor will display `Yes!!`. This is because the string is not empty it contains some text, so the statement evaluates the condition to `true`, and because the if has `true`, the else statement would not be displayed.

  - ### **[] or {} (an empty array or object)**

    ``` Js
    let test = [];

    if(test) {
        console.log("Yes!!");
    }
    else {
        console.log("No!!");
    }
    
    test = {};
    
    if(test) {
        console.log("Yes!!");
    }
    else {
        console.log("No!!");
    }
    ```

    In both the if-else statement, Our code editor will display `Yes!!`. This is because the `test` variable is not `undefined` it contains empty object that is determined as `true`, so the statement evaluates the condition to `true`, and because the if has `true`, the else statement would not be displayed.

## **Conclusion**

Javascript is an interesting and powerful language and has some unique characteristics. In this post, we covered the truthy and falsey behavior of it. First, we discussed all the Falsey values with examples and covered Truthy values later. As mentioned every value which is not Falsey is Truthy but it can be confusing sometimes and we discuss it with some examples.